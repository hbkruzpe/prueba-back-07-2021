<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyChangeEstatus;
use App\Http\Requests\Company\CreateCompanyRequest;
use App\Models\Company;

class ChangeStatusCompanyController extends Controller
{
    /**
     * Create new company
     *
     * @param \App\Http\Requests\Company\CreateCompanyRequest $request
     */
    public function __invoke(Company $company, CompanyChangeEstatus $service)
    {
        DB::beginTransaction();
        try {

            $company = $service->handle($company);
            DB::commit();
            return response($company, 200);
        } catch (\Throwable $error) {
            DB::rollBack();
            throw $error;
        }
    }
}
