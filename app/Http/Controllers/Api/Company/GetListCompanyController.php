<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyList;

class GetListCompanyController extends Controller
{
    /**
     * Create new company
     *
     * @param \App\Http\Requests\Company\CreateCompanyRequest $request
     */
    public function __invoke(CompanyList $service)
    {
        try {
            $lists = $service->handle();
            return response($lists, 200);
        } catch (\Throwable $error) {
            throw $error;
        }
    }
}
