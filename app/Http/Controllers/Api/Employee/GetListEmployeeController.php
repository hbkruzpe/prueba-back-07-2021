<?php

namespace App\Http\Controllers\Api\Employee;

use App\Models\Company;
use App\Http\Controllers\Controller;
use Vocces\Employee\Application\EmployeeList;

class GetListEmployeeController extends Controller
{
    /**
     * Create new company
     *
     * @param \App\Http\Requests\Employee\CreateEmployeeRequest $request
     */
    public function __invoke(Company  $company, EmployeeList $service)
    {
        try {

            $lists = $service->handle($company);
            return response($lists, 200);
        } catch (\Throwable $error) {
            throw $error;
        }
    }
}
