<?php

namespace App\Http\Controllers\Api\Employee;

use App\Models\Company;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Employee\Application\EmployeeCreator;
use App\Http\Requests\Employee\CreateEmployeeRequest;

class PostCreateEmployeeController extends Controller
{
    /**
     * Create new company
     *
     * @param \App\Http\Requests\Employee\CreateEmployeeRequest $request
     */
    public function __invoke(Company  $company, CreateEmployeeRequest $request, EmployeeCreator $service)
    {
        DB::beginTransaction();
        try {
            $data = $request->validated();
            $data['id'] = Str::uuid();
            $company = $service->handle($company, $data);
            DB::commit();
            return response($company, 201);
        } catch (\Throwable $error) {
            DB::rollback();
            throw $error;
        }
    }
}
