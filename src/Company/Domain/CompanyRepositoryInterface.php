<?php

namespace Vocces\Company\Domain;

use App\Models\Company as ModelsCompany;

interface CompanyRepositoryInterface
{
    /**
     * Persist a new company instance
     *
     * @param Company $company
     *
     * @return void
     */
    public function create(Company $company): void;

    public function update(ModelsCompany $modelCompany, Company $company): void;

    public function lists();
}
