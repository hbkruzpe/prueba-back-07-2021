<?php

namespace Vocces\Company\Application;

use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\ValueObject\CompanyAddress;
use Vocces\Company\Domain\ValueObject\CompanyEmail;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyName;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Shared\Domain\Interfaces\ServiceInterface;

class CompanyList implements ServiceInterface
{
    /**
     * @var CompanyRepositoryInterface $repository
     */
    private CompanyRepositoryInterface $repository;

    /**
     * Create new instance
     */
    public function __construct(CompanyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create a new company
     */
    public function handle()
    {
        $companies = $this->repository->lists();
        return $this->convertToArray($companies);

    }

    private function convertToArray($modelCompanies) {

        $companies = [];
        foreach ($modelCompanies as $modelCompany) {
            $companie = new Company(
                new CompanyId($modelCompany->id),
                new CompanyName($modelCompany->name),
                new CompanyEmail($modelCompany->email ?? ''),
                new CompanyAddress($modelCompany->address ?? ''),
                new CompanyStatus($modelCompany->status));
            array_push($companies, $companie->toArray());
        }

        return $companies;
    }
}
