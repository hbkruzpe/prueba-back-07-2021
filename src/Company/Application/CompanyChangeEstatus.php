<?php

namespace Vocces\Company\Application;

use App\Models\Company;
use Vocces\Company\Domain\Company as CompanyDomain;
use Vocces\Company\Domain\ValueObject\CompanyAddress;
use Vocces\Company\Domain\ValueObject\CompanyEmail;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyName;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Shared\Domain\Interfaces\ServiceInterface;

class CompanyChangeEstatus implements ServiceInterface
{
    /**
     * @var CompanyRepositoryInterface $repository
     */
    private CompanyRepositoryInterface $repository;

    /**
     * Create new instance
     */
    public function __construct(CompanyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * change estatu  a new company
     */
    public function handle(Company $company)
    {
        $domainCompany = new CompanyDomain(
            new CompanyId($company->id),
            new CompanyName($company->name),
            new CompanyEmail($company->email ?? ''),
            new CompanyAddress($company->address ?? ''),
            new CompanyStatus($company->status)
        );
        $domainCompany->changeStatus();
        $this->repository->update($company, $domainCompany);

        return $domainCompany;
    }
}
