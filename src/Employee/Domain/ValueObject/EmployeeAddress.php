<?php

namespace Vocces\Employee\Domain\ValueObject;

final class EmployeeAddress
{

    private string $address;

    public function __construct(string $address)
    {
        $this->address = $address;
    }

    public function get(): string
    {
        return $this->address;
    }

    public function __toString()
    {
        return $this->address;
    }
}
