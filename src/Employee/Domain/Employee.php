<?php

namespace Vocces\Employee\Domain;

use Vocces\Employee\Domain\ValueObject\EmployeeId;
use Vocces\Employee\Domain\ValueObject\EmployeeName;
use Vocces\Employee\Domain\ValueObject\EmployeeEmail;
use Vocces\Employee\Domain\ValueObject\EmployeeAddress;
use Vocces\Shared\Infrastructure\Interfaces\Arrayable;

final class Employee implements Arrayable
{
    /**
     * @var \Vocces\Employee\Domain\ValueObject\EmployeeId
     */
    private EmployeeId $id;

    /**
     * @var \Vocces\Employee\Domain\ValueObject\EmployeeName
     */
    private EmployeeName $name;

    /**
     * @var \Vocces\Employee\Domain\ValueObject\EmployeeEmail
     */
    private EmployeeEmail $email;

    /**
     * @var \Vocces\Employee\Domain\ValueObject\EmployeeAddress
     */
    private EmployeeAddress $address;

    public function __construct(
        EmployeeId      $id,
        EmployeeName    $name,
        EmployeeEmail   $email,
        EmployeeAddress $address
    ) {
        $this->id      = $id;
        $this->name    = $name;
        $this->email   = $email;
        $this->address = $address;
    }

    public function id(): EmployeeId
    {
        return $this->id;
    }

    public function name(): EmployeeName
    {
        return $this->name;
    }

    public function email(): EmployeeEmail
    {
        return $this->email;
    }

    public function address(): EmployeeAddress
    {
        return $this->address;
    }


    public function toArray()
    {
        return [
            'id'       => $this->id()->get(),
            'name'     => $this->name()->get(),
            'email'    => $this->email()->get(),
            'address'  => $this->address()->get(),
        ];
    }
}
