<?php

namespace Vocces\Employee\Domain;

use App\Models\Company as ModelsCompany;

interface EmployeeRepositoryInterface
{
    /**
     * Persist a new company instance
     *
     * @param Employee $employee
     *
     * @return void
     */
    public function create(ModelsCompany $modelCompany, Employee $employee): void;

    public function update(ModelsCompany $modelCompany, Employee $company): void;

    public function lists(ModelsCompany $modelCompany);
}
