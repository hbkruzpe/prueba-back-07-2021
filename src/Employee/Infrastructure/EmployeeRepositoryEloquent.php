<?php

namespace Vocces\Employee\Infrastructure;

use App\Models\Company;
use Vocces\Employee\Domain\Employee;
use Vocces\Employee\Domain\EmployeeRepositoryInterface;

class EmployeeRepositoryEloquent implements EmployeeRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(Company $company, Employee $employee): void
    {
        $company->employees()->create([
            'id'     => $employee->id(),
            'name'   => $employee->name(),
            'email'   => $employee->email(),
            'address'   => $employee->address(),
        ]);
    }

    /**
     * @inheritDoc
     */
    public function update(Company $company, Employee $employee): void
    {
        $company->employees()->update([
            'id'     => $employee->id(),
            'name'   => $employee->name(),
            'email'   => $employee->email(),
            'address'   => $employee->address(),
        ]);
    }

    public function lists(Company $company)
    {
       $company->load('employees');
       return $company->employees;
    }
}
