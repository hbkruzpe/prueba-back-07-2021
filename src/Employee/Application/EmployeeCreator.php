<?php

namespace Vocces\Employee\Application;

use App\Models\Company;
use Vocces\Employee\Domain\Employee;
use Vocces\Employee\Domain\ValueObject\EmployeeAddress;
use Vocces\Employee\Domain\ValueObject\EmployeeEmail;
use Vocces\Employee\Domain\ValueObject\EmployeeId;
use Vocces\Employee\Domain\ValueObject\EmployeeName;
use Vocces\Employee\Domain\EmployeeRepositoryInterface;
use Vocces\Shared\Domain\Interfaces\ServiceInterface;

class EmployeeCreator implements ServiceInterface
{
    /**
     * @var EmployeeRepositoryInterface $repository
     */
    private EmployeeRepositoryInterface $repository;

    /**
     * Create new instance
     */
    public function __construct(EmployeeRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create a new company
     */
    public function handle(Company $company, $data)
    {
        $employee = new Employee(
            new EmployeeId($data['id']),
            new EmployeeName($data['name']),
            new EmployeeEmail($data['email'] ?? ''),
            new EmployeeAddress($data['address'] ?? '')
        );

        $this->repository->create($company, $employee);

        return $employee;
    }
}
