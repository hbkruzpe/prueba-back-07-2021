<?php

namespace Vocces\Employee\Application;

use App\Models\Company;
use Vocces\Employee\Domain\Employee;
use Vocces\Employee\Domain\ValueObject\EmployeeAddress;
use Vocces\Employee\Domain\ValueObject\EmployeeEmail;
use Vocces\Employee\Domain\ValueObject\EmployeeId;
use Vocces\Employee\Domain\ValueObject\EmployeeName;
use Vocces\Employee\Domain\EmployeeRepositoryInterface;
use Vocces\Shared\Domain\Interfaces\ServiceInterface;

class EmployeeList implements ServiceInterface
{
    /**
     * @var EmployeeRepositoryInterface $repository
     */
    private EmployeeRepositoryInterface $repository;

    /**
     * Create new instance
     */
    public function __construct(EmployeeRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create a new company
     */
    public function handle(Company  $company)
    {
        $employees = $this->repository->lists($company);
        return $this->convertToArray($employees);

    }

    private function convertToArray($modelEmployees) {

        $employees = [];
        foreach ($modelEmployees as $modelEmployee) {
            $employee = new Employee(
                new EmployeeId($modelEmployee->id),
                new EmployeeName($modelEmployee->name),
                new EmployeeEmail($modelEmployee->email ?? ''),
                new EmployeeAddress($modelEmployee->address ?? ''));
            array_push($employees, $employee->toArray());
        }

        return $employees;
    }
}
