<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function(Blueprint $table) {
            $table->string('email', 100)
                  ->after('name')
                  ->nullable()->default(null);
            $table->string('address')
                  ->after('email')
                  ->nullable()->default(null);
            $table->tinyInteger('status')
                ->after('address')->default(2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function(Blueprint $table) {
            $table->dropColumn(['email', 'address']);
        });
    }
}
